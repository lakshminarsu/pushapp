import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-second',
  templateUrl: './second.page.html',
  styleUrls: ['./second.page.scss'],
})
export class SecondPage implements OnInit {
  registrationId: string='';
  constructor(private route: ActivatedRoute) { 
    this.registrationId = this.route.snapshot.params['token'];
  }

  ngOnInit() {
  }

}
